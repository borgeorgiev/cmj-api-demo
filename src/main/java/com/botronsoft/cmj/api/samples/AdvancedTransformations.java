package com.botronsoft.cmj.api.samples;

import java.io.File;
import java.net.URL;
import java.util.Collections;
import java.util.List;

import javax.annotation.Nullable;

import org.junit.runner.RunWith;

import com.botronsoft.jira.rollout.model.jiraconfiguration.issuedata.JiraIssueRoot;
import com.botronsoft.pso.cmj.modelprocessor.api.runner.JUnitSnapshotProcessorRunner;
import com.botronsoft.pso.cmj.modelprocessor.api.runner.annotations.PostTransform;
import com.botronsoft.pso.cmj.modelprocessor.impl.dsl.AbstractTransformationSpec;
import com.google.inject.Inject;

@RunWith(JUnitSnapshotProcessorRunner.class)
public class AdvancedTransformations extends AbstractTransformationSpec {

	// @SnapshotProvider
	public static List<File> describeTargetSnapshot() throws Exception {
		URL resource = AdvancedTransformations.class.getClassLoader().getResource("c6.zip");
		return Collections.singletonList(new File(resource.toURI()));
	}

	// append unwanted values from custom as a table to description field. Something like:
	// NB! This is not guaranteed to continue working in the future
	@Inject(optional = true)
	@Nullable
	private JiraIssueRoot root;

	@PostTransform
	public void modifyData() {
		if (root == null) {
			return;
		}
		root.getIssueContainers().forEach(container -> {
			container.getIssues().forEach(issue -> {
				// You can do modifications of issues here, but you need to understand the internal
				// model very well otherwise there's a good chance of breaking the model

				// E.g. set assignee if assignee is null - might be useful in cases where the target
				// system does not allow unassigned issues globally
				if (issue.getAssignee() == null) {
					issue.setAssignee(container.getProject().getLead());
				}

			});
		});
	}
}
