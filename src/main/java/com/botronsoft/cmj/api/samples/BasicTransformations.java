package com.botronsoft.cmj.api.samples;

import java.io.File;
import java.net.URL;

import org.junit.runner.RunWith;

import com.botronsoft.pso.cmj.modelprocessor.api.runner.JUnitSnapshotProcessorRunner;
import com.botronsoft.pso.cmj.modelprocessor.api.runner.annotations.SnapshotProvider;
import com.botronsoft.pso.cmj.modelprocessor.api.runner.annotations.Transform;
import com.botronsoft.pso.cmj.modelprocessor.impl.dsl.AbstractTransformationSpec;

@RunWith(JUnitSnapshotProcessorRunner.class)
public class BasicTransformations extends AbstractTransformationSpec {

	/**
	 * This method provides the snapshot to be transformed. You should return a {@link File}
	 * instance which is pointing to either ZIP snapshot or XML snapshot.
	 * <p>
	 * The transformed snapshot will be created in the same directory as the source snapshot. Look
	 * at the console log for more information.
	 * </p>
	 * <p>
	 * In this example the snapshot is read from the classpath (the file from src/main/resources
	 * will be copied by Eclipse in target/classes/ and the transformed file will also be located
	 * under target/classes)
	 */
	@SnapshotProvider
	public static File describeTargetSnapshot() throws Exception {
		URL resource = BasicTransformations.class.getClassLoader().getResource("snapshots/c6.zip");
		return new File(resource.toURI());
	}

	@Transform
	public void renameProject() {
		transform().project(project().byKey("ACINS")).renameTo("New Project for ACINS");
	}

	@Transform
	public void bulkOperations() {
		// You can bulk rename objects - e.g. prefix all Workflow Schemes to make sure they will be
		// created as new on the target system
		workflowScheme().all().forEach(scheme -> {
			transform().object(scheme).renameTo("SRC: " + scheme.get().getName());
		});

	}

	// general mapping to new global values such as priorities, Roles, Resolutions, Statuses.
	@Transform
	public void mapping() {
		// We can "merge" resolutions into other resolutions which will replace all references to
		// the old resolution with the new one.
		transform().resolution(resolution().byName("Declined"))
				.mergeTo(resolution().byName("Done"));

		// If you want to "map" a resolution from source to an existing resolution on target it's
		// enough to rename the resolution, so the name matches target
		transform().resolution(resolution().byName("Won't Do")).renameTo("Rejected");

		// You can rename priorities as well. The lines below show that you can lookup objects by
		// their original name even if they're renamed by the transformation.
		// Note that here we rename the priority to P6 and on the next line we still look it up by
		// the old name - "Minor"
		transform().priority(priority().byName("Minor")).renameTo("P6");

		transform().priority(priority().byNames("Low", "Lowest"))
				.mergeTo(priority().byName("Minor"));

		// Roles
		// Merging roles is not yet supported - you can still rename them if you need to match to
		// existing target roles.
		transform().role(role().byName("Developers")).renameTo("Engineers");

		// Merging statuses is not supported at the moment as it has many potential problems
		// related to it. Right now we would recommend cleaning up statuse manually on the source
		// system if necessary.

		// Exchange old Issue Types with new ones e.g. "Project Risk" --> "Risk"
		// This is currently only possible through rename of the issue type. There is no "safe"
		// implementation for merging many-to-one issue types as this might have complex impact on
		// workflow schemes, custom field contexts, etc.
	}

	// merge custom field values to one custom field
	@Transform
	public void mergeFields() {
		transform().field(cf().byName("Subject1")).merge().to(cf().byName("Subject2")).add();

		// exchange custom fields with system fields e.g. "Business Estimation" --> "Story Points"
		transform().field(cf().byName("Business Estimation")).merge()
				.to(cf().byName("Story Points")).add();
	}

}
