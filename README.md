# Project Title

Configuration Manager API samples

## License

This project is licensed under the Apache 2.0 License - see the [LICENSE.md](LICENSE.md) file for details

## Quick Start Guide
1. Clone the repository locally
2. Import in Eclipse as "Existing Maven projects" [more info](https://www.eclipse.org/downloads/packages/)
2. Update the pom.xml file - change the <cmj.version> property to match your CMJ version (if you're using two different versions then put the newest one)
3. Make sure the project does not have any compilation or other errors. 
3. Run BasicTransformations.java as JUnit test from Eclipse.

## Detailed Guide

TODO: ....